try {
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\"jsonrpc\":\"2.0\",\"method\":\"GetTrainDelaySimple\",\"params\":[],\"id\":2}");
            Request request = new Request.Builder()
                    .url("http://mapa.zsr.sk/json.rpc")
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "642672b0-2cab-42f4-a001-093f99baf69c")
                    .build();

            okhttp3.Response response =  client.newCall(request).execute();
            

        } catch (IOException e) {
            e.printStackTrace();
        }