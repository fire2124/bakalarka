const fs = require("fs");
const fetch = require("node-fetch");

if (!fs.existsSync("./TrafficSituation")) {
    fs.mkdirSync("./TrafficSituation");
}

function downloadTrafficSituation() {
    var request = require("request");

    var options = {
        method: 'GET',
        url: 'https://portal.minv.sk/wps/portal/domov/sluzby_pz/dopravne_krizove_situacie/%21ut/p/a1/jZBND8FAEIZ_TY_tDqqW2_qIqo8QRO1FFqs2arfZXT349VpxcKBMMsnM5HnfvBlEUYyoZLlImBVKsrTcabDzoTcIozFEw_WsAaQdLAarJa7jZa0AtgXQG5LQb00AwMd1GPW7Yb_VngKMgv_08KUI_NJvEK1Exs0XUBXxCVRkiBBNUrV__mNL5L6BE0Q1P3HNtXfTxflsbWY6DjhwZJZ5ico9c3Gg7PJguC0GlWmWS-4y96LFXeXcNcLe2EFwV8lUSP7J_qyMRfGbK8qu6xjE_LrBhjwAjf7LfQ%21%21/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_40CEHJK0JGUN30A96QETS828Q4/res/id=QCPgetSituations/c=cacheLevelPage/=/',
        qs: {
            regionId: '0',
            categories: 'ZD6;ZD7;ZD8;ZD1;ZD2;ZD3;ZD5',
            showPlanned: 'false',
            fbclid: 'IwAR3J1JZWD2eAWA--cwVMkiPzMvrrzJYQkP7F5ExcaT6whpedKi8NkCu0ATE'
        },
        json: true
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        const date = new Date();
        const imageDate = Date.parse(date);

        Object.size = function (obj) {
            var size = 0,
                key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };

        // Get the size of an object
        var size = Object.size(body.situations);
        console.log(size)
        let numbers=0;
        let array = [];
        let globalObject = {};
        for (let i = 0; i < size; i++) {
    
           try {
                console.log(i)
                console.log(body.situations[i].district.parent.name)
                if ((body.situations[i].district.parent.name === "Košický kraj") ||
                (body.situations[i].district.parent.name === "Prešovský kraj")) {
                array.push(body.situations[i]);
                numbers++;
            }
           } catch (error) {  
           }     
        }
        console.log(array)
        fs.writeFileSync(`./TrafficSituation/${imageDate}.json`, JSON.stringify(array));
        console.log(numbers)
    });
}

module.exports =setInterval(downloadTrafficSituation, 5000);

