package com.example.filip.schoolproject.BussDb;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.filip.schoolproject.Activity.ActivityOne;

import java.lang.ref.WeakReference;

public class DbTools {
    private static BusDatabase _db;
    public DbTools(WeakReference<Context> refContext){
        getDbContext(refContext);
    }
    public static BusDatabase getDbContext(WeakReference<Context> refContext){
        if(_db!=null)
            return _db;
        return Room.databaseBuilder(refContext.get(), BusDatabase.class,"movie-db").build();
    }
}