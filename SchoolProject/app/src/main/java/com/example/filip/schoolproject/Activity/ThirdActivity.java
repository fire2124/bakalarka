package com.example.filip.schoolproject.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.filip.schoolproject.Markers.BussMarker;
import com.example.filip.schoolproject.Markers.TrainMarker;
import com.example.filip.schoolproject.R;
import com.example.filip.schoolproject.RestApi.ApiMethods;
import com.example.filip.schoolproject.RestApi.ApiTools;
import com.example.filip.schoolproject.RestApi.Buss;
import com.example.filip.schoolproject.RestApi.Train;
import com.google.gson.JsonObject;

import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ThirdActivity extends AppCompatActivity  {
//implements LocationListener
    private List<Buss> zoznamBusov;
    private List<Train> zoznamVlakov;

    public List<BussMarker> zoznamBussMarks;
    public List<TrainMarker> zoznamTrainMarks;
    public TextView locationText;
    public LocationManager locManager;
    public final int PERMISSION_LOCATION_ID = 1000;
    private final String CHANNEL_ID = "personal_notifications";
    private final int NOTIFICATION_ID = 1;
    public String[] number;
    public int count, counter,counter2;


   // Marker startMarkerBuss, startMarkerTrain;
    BussMarker startMarkerBuss;
    TrainMarker startMarkerTrain;
    IMapController controller;
    MapView map;
    Overlay startMarkerBussZoznam;


    private final int PERMISSION_WRITE_EXT_STORAGE_ID = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third_activity);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WRITE_EXT_STORAGE_ID);
        else
            //inicializacia mapy
            initialMapSetup();

        //lockManager();
        trafficAct();
        layoutChange();
        train();
        buss();
    }


    public void buss() {
        Thread thread = new Thread() {
            public void run() {
                new Timer().scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl("https://mhdpresov.sk/").build();
                        ApiMethods api = retrofit.create(ApiMethods.class);
                        api.getGPSBusses().enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                zoznamBusov = ApiTools.getBussesFromApi(response.body(), ThirdActivity.this);
                                //  rg();
                                if (startMarkerBuss != null) {
                                    for (; counter >= 0; counter--) {
                                        try {
                                                //map.getOverlays().remove(startMarkerBuss);- odstrani jeden
                                                //map.getOverlays().removeIf((Predicate<? super Overlay>) startMarkerBuss);
                                                //map.getOverlays().removeIf(startMarkerBuss);
                                                //map.getOverlays().remove(Collections.singleton(startMarkerBuss));//- odstrani vsetky
                                                //map.getOverlays().remove(j);
                                                //map.getOverlays().clear(startMarkerBuss);
                                                    //map.getOverlays().retainAll(Collections.singleton(startMarkerBuss));
                                                //map.getOverlays().removeAll(Collections.singleton(startMarkerBuss));
                                               // map.getOverlayItem()
                                                    map.getOverlays().remove(counter);

                                                } catch (NullPointerException e) {
                                                    System.out.println(e);
                                                }catch (IndexOutOfBoundsException e){
                                                     System.out.println(e);
                                                }
                                        }
                                    }

//                                Iterator overlayIterator = map.getOverlays().iterator();
//                                while (overlayIterator.hasNext()){
//                                    Object overlay = overlayIterator.next();
//                                    if (overlay instanceof BussDatabase){
//                                        overlayIterator.remove();
//                                    }
//                                }
//                                for (Buss bus: zoznamBusov) {
//                                    //controller.setCenter(new GeoPoint( zoznamBusov.get(j).getLat(), zoznamBusov.get(j).getLng()));//musi tu byt aby bolo viditelny pohyb vozidiel
//                                    BussMarker startMarkerBuss = new BussMarker(map,0);
//                                    startMarkerBuss.setIcon(getResources().getDrawable(R.drawable.bus_black_24dp));
//                                    startMarkerBuss.setRotation(bus.getDirection());
//                                    startMarkerBuss.setTitle("číslo autobusu: " + bus.getNumber() + "\n" +
//                                            "meškanie : " + bus.getDelay() + "\n" +
//                                            "príchod: " + bus.getDeparture() + "\n" +
//                                            "lat: " + bus.getLat().toString() + "\n" +
//                                            "lng: " + bus.getLng().toString());
//                                    startMarkerBuss.setPosition(new GeoPoint(bus.getLat(), bus.getLng()));
//                                    startMarkerBuss.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
//                                    map.getOverlayManager().add(startMarkerBuss);
//                                    sizer++;
//                                }
                                for (int k = 0; k < zoznamBusov.size(); k++) {
                                    try {
                                        //controller.setCenter(new GeoPoint( zoznamBusov.get(j).getLat(), zoznamBusov.get(j).getLng()));//musi tu byt aby bolo viditelny pohyb vozidiel
                                        startMarkerBuss = new BussMarker(map, 0);
                                        startMarkerBuss.setIcon(getResources().getDrawable(R.drawable.bus_black_24dp));
                                        startMarkerBuss.setRotation(zoznamBusov.get(k).getDirection());
                                        startMarkerBuss.setTitle("číslo autobusu: " + zoznamBusov.get(k).getNumber() + "\n" +
                                                "meškanie : " + zoznamBusov.get(k).getDelay() + "\n" +
                                                "príchod: " + zoznamBusov.get(k).getDeparture() + "\n" +
                                                "lat: " + zoznamBusov.get(k).getLat().toString() + "\n" +
                                                "lng: " + zoznamBusov.get(k).getLng().toString());
                                        startMarkerBuss.setPosition(new GeoPoint(zoznamBusov.get(k).getLat(), zoznamBusov.get(k).getLng()));
                                        startMarkerBuss.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);

                                          map.getOverlays().add(k,startMarkerBuss);
                                        counter++;

                                        //map.getOverlayManager().add(startMarkerBuss);
                                    } catch (NullPointerException e) {
                                        System.out.println(e);
                                    }
                                }

                                //map.getOverlays().add(startMarkerBussZoznam);
                                count++;
                                Intent intent = getIntent();
                                intent.putExtra("count", count);
                                //showNotification();
                            }
                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                Toast.makeText(ThirdActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                                Log.v("", "");
                            }
                        });
                    }
                }, 0, 15000);
            }
        };
        thread.start();
    }

    public void train() {
        Thread thread2 = new Thread() {
            public void run() {
                new Timer().scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                        httpClient.addInterceptor(new Interceptor() {
                            @Override
                            public okhttp3.Response intercept(Chain chain) throws IOException {
                                Request original = chain.request();
                                MediaType mediaType = MediaType.parse("application/octet-stream");
                                RequestBody body = RequestBody.create(mediaType, "{\"jsonrpc\":\"2.0\",\"method\":\"GetTrainDelaySimple\",\"params\":[],\"id\":1}");
                                Request request = original.newBuilder()
                                        .post(body)
                                        .addHeader("cache-control", "no-cache")
                                        .build();
                                return chain.proceed(request);
                            }
                        });
                        OkHttpClient client = httpClient.build();
                        Retrofit retrofit2 = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                                .client(client).baseUrl("https://mapa.zsr.sk/").build();
                        ApiMethods api2 = retrofit2.create(ApiMethods.class);
                        api2.getTrains()
                                .enqueue(new Callback<JsonObject>() {
                                    @Override
                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                        JsonObject obj = response.body().getAsJsonObject();
                                        zoznamVlakov = ApiTools.getTrainFromApi(obj, ThirdActivity.this);

                                        if (startMarkerTrain != null) {
                                            for (; counter2+zoznamBusov.size()+1 >= zoznamBusov.size()+1; counter2--) {
                                                try {

                                                    map.getOverlays().remove(counter2+zoznamBusov.size());

                                                } catch (NullPointerException e) {
                                                    System.out.println(e);
                                                }catch (IndexOutOfBoundsException e){
                                                    System.out.println(e);
                                                }
                                            }
                                        }

                                        for (int l = 0; l < zoznamVlakov.size(); l++) {
                                            try {
                                                //if (zoznamVlakov.get(l).getLg() >= 20.4595161) {
                                                    //controller.setCenter(new GeoPoint(zoznamVlakov.get(j).getLat(), zoznamVlakov.get(j).getLng()));
                                                    // musi tu byt aby bolo viditelny pohyb vozidiel
                                                    startMarkerTrain = new TrainMarker(map,1);
                                                    startMarkerTrain.setIcon(getResources().getDrawable(R.drawable.train_black_foreground));
                                                    startMarkerTrain.setTitle(
                                                            "názov vlaku: " + zoznamVlakov.get(l).getNazov() + "\n" +
//                                                                "dopravca: " + zoznamVlakov.get(j).getDopravca() + "\n" +
                                                                    "popis: " + zoznamVlakov.get(l).getPopis() + "\n" +
                                                                    "meškanie: " + zoznamVlakov.get(l).getMeska() + "\n" +
                                                                    "meška text: " + zoznamVlakov.get(l).getMeskaText() + "\n" +
//                                                                "cas: " + zoznamVlakov.get(i).getCas() + "\n" +
//                                                                "casovy plan: " + zoznamVlakov.get(i).getCasPlan() + "\n" +
                                                                    "príchod: " + zoznamVlakov.get(l).getInfoZoStanice() + "\n" +
                                                                    "lat: " + zoznamVlakov.get(l).getLt().toString() + "\n" +
                                                                    "lng: " + zoznamVlakov.get(l).getLg().toString());
                                                    startMarkerTrain.setPosition(new GeoPoint(zoznamVlakov.get(l).getLt(), zoznamVlakov.get(l).getLg()));
                                                    startMarkerTrain.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                                                    map.getOverlays().add(zoznamBusov.size()+l,startMarkerTrain);
                                                    counter2++;

                                               // }
                                            } catch (NullPointerException e) {
                                                System.out.println(e);
                                            }
                                        }

                                        counter++;
                                        Intent intent = getIntent();
                                        intent.putExtra("counter", counter);
                                        Log.v("", "");

                                    }
                                    @Override
                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                       // Toast.makeText(ThirdActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                                        Log.v("", t.getMessage());
                                    }
                                });
                    }
                }, 0, 15000);
            }
        };
        thread2.start();
    }

    private void trafficAct() {
        (findViewById(R.id.traffic)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(ThirdActivity.this, DelayActivity.class);
                startActivity(i);
            }
        });
    }

    public void layoutChange() {

        //zmena layoutov
        (findViewById(R.id.imageButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(ThirdActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

        (findViewById(R.id.imageButton2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(ThirdActivity.this, SecondActivity.class);
                startActivity(i);
            }
        });
        (findViewById(R.id.imageButton3)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(ThirdActivity.this, ThirdActivity.class);
                startActivity(i);
            }
        });
    }

    private void initialMapSetup() {
        map = findViewById(R.id.map_osm);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        controller = map.getController();
        controller.setZoom(14.0d);
        controller.setCenter(new GeoPoint(48.9970342, 21.2404656));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (map != null)
            map.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (map != null)
            map.onResume();
    }
}



