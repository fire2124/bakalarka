package com.example.filip.schoolproject.RestApi;

import com.google.gson.JsonArray;

import java.util.List;

public class Train {

    public String nazov;
    public String popis;
    public int meska;
    public String meskaColor;
    public String meskaText;
    public String meskaTextColor;
    public String infoZoStanice;
    public String cas;
    public String casPlan;
    public String dopravca;
    public String poznamka;
    public JsonArray position;
    public Double lt;
    public Double lg;
    public String key;
    public List<Double[]> value;

    public String getNazov() {
        return nazov;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public String getPopis() {
        return popis;
    }

    public void setPopis(String popis) {
        this.popis = popis;
    }

    public int getMeska() {
        return meska;
    }

    public void setMeska(int meska) {
        this.meska = meska;
    }

    public String getMeskaColor() {
        return meskaColor;
    }

    public void setMeskaColor(String meskaColor) {
        this.meskaColor = meskaColor;
    }

    public String getMeskaText() {
        return meskaText;
    }

    public void setMeskaText(String meskaText) {
        this.meskaText = meskaText;
    }

    public String getMeskaTextColor() {
        return meskaTextColor;
    }

    public void setMeskaTextColor(String meskaTextColor) {
        this.meskaTextColor = meskaTextColor;
    }

    public String getInfoZoStanice() {
        return infoZoStanice;
    }

    public void setInfoZoStanice(String infoZoStanice) {
        this.infoZoStanice = infoZoStanice;
    }

    public String getCas() {
        return cas;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    public String getCasPlan() {
        return casPlan;
    }

    public void setCasPlan(String casPlan) {
        this.casPlan = casPlan;
    }

    public String getDopravca() {
        return dopravca;
    }

    public void setDopravca(String dopravca) {
        this.dopravca = dopravca;
    }

    public String getPoznamka() {
        return poznamka;
    }

    public void setPoznamka(String poznamka) {
        this.poznamka = poznamka;
    }


    public JsonArray getPosition() {
        return position;
    }

    public void setPosition(JsonArray position) {
        this.position = position;
    }

    public Double getLt() {
        return lt;
    }

    public void setLt(Double lt) {
        this.lt = lt;
    }

    public Double getLg() {
        return lg;
    }

    public void setLg(Double lg) {
        this.lg = lg;
    }
}



