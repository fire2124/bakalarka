package com.example.filip.schoolproject.BussDb;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;


@Database(entities = {BussDatabase.class}, version = 1 ,exportSchema = false)
public abstract class BusDatabase extends RoomDatabase {
    public abstract BusDao bussDao();
}