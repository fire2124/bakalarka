package com.example.filip.schoolproject.Markers;

import android.content.Context;

import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

public class TrainMarker extends Marker {

    private Integer train;

    public TrainMarker(MapView mapView, Integer train) {
        super(mapView);
        this.train = train;

    }

}
