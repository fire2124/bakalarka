package com.example.filip.schoolproject.BussDb;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Buss {

    public Buss(){

    }
    public Buss(String title, String startStop, String endStop){
        setTitle(title);
        setStartStop(startStop);
        setEndStop(endStop);
    }

    @PrimaryKey(autoGenerate = true)

    private long Id;
    @ColumnInfo(name="title")
    private String Title;
    @ColumnInfo(name="startStop")
    private String StartStop;
    @ColumnInfo(name="endStop")
    private String EndStop;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getStartStop() {
        return StartStop;
    }

    public void setStartStop(String startStop) {
        StartStop = startStop;
    }

    public String getEndStop() {
        return EndStop;
    }

    public void setEndStop(String endStop) {
        EndStop = endStop;
    }
    //Getters
//
//    public Buss(){
//
//    }
//
//    public Buss(String title,String konecnazastavka1,String konecnazastavka2){
//        setTitle(title);
//        setKonecnazastavka1(konecnazastavka1);
//        setKonecnazastavka2(konecnazastavka2);
//    }
//
//
//    @PrimaryKey(autoGenerate = true)
//
//    private long Id;
//    @ColumnInfo(name="title")
//    private String Title;
//    @ColumnInfo(name="konecna_zastavka_1")
//    private String Konecnazastavka1;
//    @ColumnInfo(name="konecna_zastavka_2")
//    private String Konecnazastavka2;
//
//
//
//
//
//    public long getId() {
//        return Id;
//    }
//    public void setId(long id) {
//        Id = id;
//    }
//
//    public String getTitle() {
//        return Title;
//    }
//    public void setTitle(String title) {
//        Title = title;
//    }
//
//
//    public String getKonecnazastavka1() {
//        return Konecnazastavka1;
//    }
//    public void setKonecnazastavka1(String konecnazastavka1) {
//        Konecnazastavka1 = konecnazastavka1;
//    }
//
//    public String getKonecnazastavka2() {
//        return Konecnazastavka2;
//    }
//    public void setKonecnazastavka2(String konecnazastavka2) {
//        Konecnazastavka2 = konecnazastavka2;
//    }
    //Getters
}
