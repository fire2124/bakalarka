package com.example.filip.schoolproject.RestApi;


import com.google.gson.JsonObject;
import com.google.gson.annotations.JsonAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;


public interface ApiMethods {
    @GET("getGPSBusses")
    Call<JsonObject> getGPSBusses();

    @Headers({"Content-Type: application/json"})
    @POST("json.rpc")
    Call<JsonObject> getTrains();


}



