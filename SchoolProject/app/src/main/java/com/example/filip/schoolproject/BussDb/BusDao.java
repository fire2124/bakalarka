package com.example.filip.schoolproject.BussDb;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface BusDao {
    @Query("SELECT * FROM BussDatabase")
    List<BussDatabase> getAll();

    @Query("SELECT * FROM BussDatabase WHERE Id LIKE :Id")
    BussDatabase getById(int Id);

    @Insert
    void insertBuss(BussDatabase... busses);

    @Delete
    void deleteMovie(BussDatabase buss);
}