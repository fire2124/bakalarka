package com.example.filip.schoolproject.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.filip.schoolproject.R;
import com.example.filip.schoolproject.RestApi.ApiMethods;
import com.example.filip.schoolproject.RestApi.ApiTools;
import com.example.filip.schoolproject.RestApi.Buss;
import com.google.gson.JsonObject;

import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TrafficActivity extends AppCompatActivity implements LocationListener {

    private List<Buss> zoznamBusov;
    public TextView locationText;
    public LocationManager locManager;
    public final int PERMISSION_LOCATION_ID = 1000;
    private final String CHANNEL_ID = "personal_notifications";
    private final int NOTIFICATION_ID = 1;

    public String[] number;
    public int count;
    Marker mark;
    IMapController controller;
    MapView map;
    int j;
    private final int PERMISSION_WRITE_EXT_STORAGE_ID = 1000;
    private final int PERMISSION_FINE_LOC_ID = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third_activity_traffic);


        layoutChange();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WRITE_EXT_STORAGE_ID);
        else
            //inicializacia mapy
            initialMapSetup();

        //lockManager();


        Thread thread = new Thread() {
            public void run() {
                new Timer().scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {

                        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).
                                baseUrl("https://mhdpresov.sk/").build();
                        ApiMethods api = retrofit.create(ApiMethods.class);
                        api.getGPSBusses().enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                zoznamBusov = ApiTools.getBussesFromApi(response.body(), TrafficActivity.this);


                                for (j = 0; j < zoznamBusov.size(); j++) {
                                    try {
                                        controller.setCenter(new GeoPoint(zoznamBusov.get(j).getLat(), zoznamBusov.get(j).getLng()));//musi tu byt aby bolo viditelny pohyb vozidiel
                                        rog();

                                    } catch (NullPointerException e) {
                                        System.out.println(e);
                                    }

                                }


                                count++;
                                Intent intent = getIntent();
                                intent.putExtra("count", count);
                                Log.v("", "");
                                //showNotification();

                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                Toast.makeText(TrafficActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }, 0, 15000);
            }
        };

        thread.start();


    }

    private void lockManager() {
        locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION_ID);
            return;
        }
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5, 5, this);
    }

    public void layoutChange() {

        //zmena layoutov
        (findViewById(R.id.imageButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(TrafficActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

        (findViewById(R.id.imageButton2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(TrafficActivity.this, SecondActivity.class);
                startActivity(i);
            }
        });
        (findViewById(R.id.imageButton3)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(TrafficActivity.this, ThirdActivity.class);
                startActivity(i);
            }
        });
    }

    private void initialMapSetup() {
        map = findViewById(R.id.map_osm);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        controller = map.getController();
        controller.setZoom(14.0d);
        controller.setCenter(new GeoPoint(48.9970342, 21.2404656));
        //49°00′00″S 21°14′00″V


        // List<Marker> startMarkerBuss= new ArrayList<Marker>((Collection<? extends Marker>) map);
        //Creating a list of markers
        //  for(int i = 0 ; i<zoznamBusov.size();i++) {
//            Marker startMarkerBuss = new Marker(map);
//            startMarkerBuss.setIcon(getResources().getDrawable(R.drawable.bus_black_25dp));
//            startMarkerBuss.setRotation(direction);
//            startMarkerBuss.setTitle(number);
//            startMarkerBuss.setPosition(new GeoPoint(lat, lon));
//            startMarkerBuss.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
//            map.getOverlays().add(startMarkerBuss);

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                Marker startMarkerBuss = new Marker(map);
//                startMarkerBuss.setIcon(getResources().getDrawable(R.drawable.bus_black_25dp));
//                startMarkerBuss.setRotation(direction);
//                startMarkerBuss.setTitle(number);
//                startMarkerBuss.setPosition(new GeoPoint(lat, lon));
//                startMarkerBuss.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
//                map.getOverlays().add(startMarkerBuss);
//
//
//            }
//        }, SPLASH_TIME);

//        new Timer().scheduleAtFixedRate(new TimerTask(){
//            @Override
//            public void run(){
//                Marker startMarkerBuss = new Marker(map);
//                startMarkerBuss.setIcon(getResources().getDrawable(R.drawable.bus_black_25dp));
//                startMarkerBuss.setRotation(direction);
//                startMarkerBuss.setTitle(number);
//                startMarkerBuss.setPosition(new GeoPoint(lat, lon));
//                startMarkerBuss.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
//                map.getOverlays().add(startMarkerBuss);
//            }
//        },0,15000);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (map != null)
            map.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (map != null)
            map.onResume();
    }

    @Override
    public void onLocationChanged(Location location) {
        locationText.setText("Lat:" + Double.toString(location.getLatitude()) + " Lon:" + Double.toString(location.getLongitude()));
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(this, "GPS bolo zapnute", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(this, "GPS bolo vypnute", Toast.LENGTH_LONG).show();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_LOCATION_ID: {
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5, 5, this);
                        } else {
                            Toast.makeText(this, "Unable to get proper permission. You sure?", Toast.LENGTH_LONG).show();
                            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION_ID);
                        }
                    }
                }
                break;
            }
        }
    }


    public void rog() {
        mark = new Marker(map);
        if (zoznamBusov.get(j).getDelay() > 0) {
            mark.setIcon(getResources().getDrawable(R.drawable.ic_launcher_background));
        } else if (zoznamBusov.get(j).getDelay() >= -120) {
            mark.setIcon(getResources().getDrawable(R.drawable.ic_launcher_background2));
        } else mark.setIcon(getResources().getDrawable(R.drawable.ic_launcher_background1));
        mark.setPosition(new GeoPoint(zoznamBusov.get(j).getLat(), zoznamBusov.get(j).getLng()));
        mark.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        map.getOverlays().add(mark);
    }
}
