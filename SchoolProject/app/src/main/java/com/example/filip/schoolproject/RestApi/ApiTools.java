package com.example.filip.schoolproject.RestApi;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class ApiTools {


    public static List<Buss> getBussesFromApi(JsonObject dataZoSluzby, Context context) {
        JsonArray buslist = dataZoSluzby.get("busses").getAsJsonArray();
        List<Buss> newBusList = new ArrayList<>();
        for (int i = 0; i < buslist.size(); i++) {
            Buss newBus = new Buss();
            newBus.number = buslist.get(i).getAsJsonObject().get("number").getAsString();
            newBus.departure = buslist.get(i).getAsJsonObject().get("departure").getAsString();
            newBus.delay = buslist.get(i).getAsJsonObject().get("delay").getAsInt();
            newBus.direction = buslist.get(i).getAsJsonObject().get("direction").getAsInt();
            newBus.lat = buslist.get(i).getAsJsonObject().get("lat").getAsDouble();
            newBus.lng = buslist.get(i).getAsJsonObject().get("lng").getAsDouble();
            newBusList.add(newBus);
        }
        return newBusList;
    }

    public static List<Train> getTrainFromApi(JsonObject dataZoSluzby2, Context context) {
        JsonArray Trainlist = dataZoSluzby2.get("result").getAsJsonArray();

        List<Train> newTrainlist = new ArrayList<>();
        for (int i = 0; i < Trainlist.size(); i++) {
            Train newTrain = new Train();
            try {
                newTrain.nazov = Trainlist.get(i).getAsJsonObject().get("Nazov").getAsString();
                newTrain.popis = Trainlist.get(i).getAsJsonObject().get("Popis").getAsString();
                newTrain.meska = Trainlist.get(i).getAsJsonObject().get("Meska").getAsInt();
//                    newTrain.meskaColor= Trainlist.get(i).getAsJsonObject().get("MeskaColor").getAsString();
                newTrain.meskaText = Trainlist.get(i).getAsJsonObject().get("MeskaText").getAsString();
//                    newTrain.meskaTextColor = Trainlist.get(i).getAsJsonObject().get("MeskaTextColor").getAsString();
                newTrain.infoZoStanice = Trainlist.get(i).getAsJsonObject().get("InfoZoStanice").getAsString();
//                    newTrain.cas = Trainlist.get(i).getAsJsonObject().get("Cas").getAsString();
//                    newTrain.casPlan = Trainlist.get(i).getAsJsonObject().get("CasPlan").getAsString();
                newTrain.dopravca = Trainlist.get(i).getAsJsonObject().get("Dopravca").getAsString();
//                    newTrain.poznamka = Trainlist.get(i).getAsJsonObject().get("Poznamka").getAsString();
//                    js=Trainlist.get(i).getAsJsonObject().get("Position").getAsJsonArray();
                newTrain.lt = Trainlist.get(i).getAsJsonObject().get("Position").getAsJsonArray().get(0).getAsDouble();
                newTrain.lg = Trainlist.get(i).getAsJsonObject().get("Position").getAsJsonArray().get(1).getAsDouble();
            } catch (Exception e) {
            }
            newTrainlist.add(newTrain);
        }
        return newTrainlist;
    }
}
