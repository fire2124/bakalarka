package com.example.filip.schoolproject.BussDb;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class BussDatabase {

    public BussDatabase(){

    }
    public BussDatabase(String title, String startStop, String endStop){
        setTitle(title);
        setStartStop(startStop);
        setEndStop(endStop);
    }

    @PrimaryKey(autoGenerate = true)
    private long Id;
    @ColumnInfo(name="title")
    private String Title;
    @ColumnInfo(name="startStop")
    private String StartStop;
    @ColumnInfo(name="endStop")
    private String EndStop;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getStartStop() {
        return StartStop;
    }

    public void setStartStop(String startStop) {
        StartStop = startStop;
    }

    public String getEndStop() {
        return EndStop;
    }

    public void setEndStop(String endStop) {
        EndStop = endStop;
    }

}
