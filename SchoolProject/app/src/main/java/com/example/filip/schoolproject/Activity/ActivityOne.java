package com.example.filip.schoolproject.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.filip.schoolproject.BussDb.BussDatabase;
import com.example.filip.schoolproject.R;
import com.example.filip.schoolproject.BussDb.DbTools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;

import static android.os.Environment.getExternalStorageDirectory;

public class ActivityOne extends AppCompatActivity {
    List<BussDatabase> insertedBusses;
    private final int PERMISSION_ID_ACCESS_STORAGE = 1000;
    private final String fileName = "zastavky.txt";
    private final String extPath = "/dataFiles";
    private int cislo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);

        layoutChange();
//        TextView txtInfo = findViewById(R.id.txtInfo);
        if (getIntent() != null) {
//            String info = getIntent().getStringExtra("info");
//           // txtInfo.setText(info);
            cislo = getIntent().getIntExtra("position", 0);
        }

        database();
    }

    private void database() {
        DbGetData taskLoadData = new DbGetData();

        BussDatabase newBuss = new BussDatabase("1", "Nižná Šebastová\n", "Šebastová\n");
        BussDatabase newBuss2 = new BussDatabase("2", "Bajkalská\n", "Budovateľská\n");
        BussDatabase newBuss3 = new BussDatabase("4", "Sídlisko III\n", "Pod Šalgovíkom\n");
        BussDatabase newBuss4 = new BussDatabase("5", "Budovateľská\n", "Bajkalská\n");
        BussDatabase newBuss5 = new BussDatabase("5D", "Obrancov mieru\n", "Dúbrava\n");
        BussDatabase newBuss6 = new BussDatabase("7", "Širpo\n", "Budovateľská\n");
        BussDatabase newBuss7 = new BussDatabase("8", "Sídlisko III\n", "Sibírska\n");
        BussDatabase newBuss8 = new BussDatabase("10", "Hollého\n", "Sibírska\n");
        BussDatabase newBuss9 = new BussDatabase("11", "Na Rúrkach\n", "Solivar\n");
        BussDatabase newBuss10 = new BussDatabase("12", "Šidlovec\n", "Sibírska\n");
        BussDatabase newBuss11 = new BussDatabase("13", "Veľká pošta\n", "Severná\n");
        BussDatabase newBuss12 = new BussDatabase("14", "Kanaš - Stráže\n", "Záborské\n");
        BussDatabase newBuss13 = new BussDatabase("15", "Za Kalváriou\n", "Nemocnica\n");
        BussDatabase newBuss14 = new BussDatabase("17", "Sídlisko III\n", "Širpo\n");
        BussDatabase newBuss15 = new BussDatabase("18", "Bzenov\n", "Divadlo J. Záborského\n");
        BussDatabase newBuss16 = new BussDatabase("19", "Hollého\n", "Solivar\n");
        BussDatabase newBuss17 = new BussDatabase("20", "Divadlo J. Záborského\n", "Veselá\n");
        BussDatabase newBuss18 = new BussDatabase("21", "Fintice\n", "Malý Šariš\n");
        BussDatabase newBuss19 = new BussDatabase("22", "Šidlovec\n", "Teriakovce\n");
        BussDatabase newBuss20 = new BussDatabase("24", "Obrancov mieru\n", "Haniska\n");
        BussDatabase newBuss21 = new BussDatabase("27", "Hviezdna\n", "Terchovská\n");
        BussDatabase newBuss22 = new BussDatabase("28", "Ľubotice\n", "Delňa\n");
        BussDatabase newBuss23 = new BussDatabase("29", "Sídlisko III\n", "Nemocnica\n");
        BussDatabase newBuss24 = new BussDatabase("30", "Železničná stanica\n", "Budovateľská\n");
        BussDatabase newBuss25 = new BussDatabase("32", "Sibírska\n", "Trojica\n");
        BussDatabase newBuss26 = new BussDatabase("32A", "Sibírska\n", "Okružná\n");
        BussDatabase newBuss27 = new BussDatabase("33", "Nižná Šebastová\n", "Šebastová\n");
        BussDatabase newBuss28 = new BussDatabase("34", "Širpo\n", "Delňa\n");
        BussDatabase newBuss29 = new BussDatabase("35", "Delňa\n", "Širpo\n");
        BussDatabase newBuss30 = new BussDatabase("36", "Trojica\n", "Trojica\n");
        BussDatabase newBuss31 = new BussDatabase("37", "Železničná stanica\n", "Nižná Šebastová\n");
        BussDatabase newBuss32 = new BussDatabase("38", "Nižná Šebastová\n", "Šebastová\n");
        BussDatabase newBuss33 = new BussDatabase("39", "Sídlisko III\n", "Lomnická\n");
        BussDatabase newBuss34 = new BussDatabase("41", "Surdok\n", "Divadlo J. Záborského\n");
        BussDatabase newBuss35 = new BussDatabase("42", "Borkút\n", "Kpt. Nálepku\n");
        BussDatabase newBuss36 = new BussDatabase("43", "Železničná stanica\n", "Kpt. Nálepku\n");
        BussDatabase newBuss37 = new BussDatabase("44", "Veľká pošta\n", "Solivar\n");
        BussDatabase newBuss38 = new BussDatabase("45", "Veľký Šariš\n", "Delňa\n");
        BussDatabase newBuss39 = new BussDatabase("46", "Veľká pošta\n", "Ruská Nová Ves\n");
        BussDatabase newBuss40 = new BussDatabase("N1", "Sídlisko III\n", "Solivar\n");
        BussDatabase newBuss41 = new BussDatabase("N2", "ídlisko III\n", "Pod Šalgovíkom\n");
        BussDatabase newBuss42 = new BussDatabase("N3", "Nižná Šebastová\n", "Sibírska\n");

        taskLoadData.execute(newBuss, newBuss2, newBuss3, newBuss4, newBuss5, newBuss6, newBuss7, newBuss8, newBuss9, newBuss10,
                newBuss11, newBuss12, newBuss13, newBuss14, newBuss15, newBuss16, newBuss17,
                newBuss18, newBuss19, newBuss20, newBuss21, newBuss22, newBuss23, newBuss24, newBuss25, newBuss26,
                newBuss27, newBuss28, newBuss29, newBuss30, newBuss31, newBuss32, newBuss33, newBuss34,
                newBuss35, newBuss36, newBuss37, newBuss38, newBuss39, newBuss40, newBuss41, newBuss42);
        //END: ROOM DB Persistence preparation
    }

    //START: ROOM DB Persistence TASKS in NEW THREAD
    class DbGetData extends AsyncTask<BussDatabase, Integer, List<BussDatabase>> {

        @Override
        protected List<BussDatabase> doInBackground(BussDatabase... busses) {
            List<BussDatabase> data = DbTools.getDbContext(new WeakReference<Context>(ActivityOne.this)).bussDao().getAll();
            if (data.size() == 0) {
                DbTools.getDbContext(new WeakReference<Context>(ActivityOne.this)).bussDao().insertBuss(busses);
                return DbTools.getDbContext(new WeakReference<Context>(ActivityOne.this)).bussDao().getAll();
            } else
                return DbTools.getDbContext(new WeakReference<Context>(ActivityOne.this)).bussDao().getAll();
        }

        @Override
        protected void onPostExecute(List<BussDatabase> busses) {
            super.onPostExecute(busses);
            insertedBusses = busses;

            ((TextView) findViewById(R.id.tv_movie)).setText("Číslo autobusu: \n" + insertedBusses.get(cislo).getTitle());
            saveFirstBussNameToSharedPreferences("dataKey", insertedBusses.get(cislo).getTitle());

            ((TextView) findViewById(R.id.tv_movie1)).setText("Začiatočná zastávka: \n" + insertedBusses.get(cislo).getStartStop()); //getStartStop()
            saveFirstBussNameToSharedPreferences("dataKey2", insertedBusses.get(cislo).getStartStop());//getStartStop()

            ((TextView) findViewById(R.id.tv_movie2)).setText("Konečná zastávka: \n" + insertedBusses.get(cislo).getEndStop());//getEndStop()
            saveFirstBussNameToSharedPreferences("dataKey3", insertedBusses.get(cislo).getEndStop());//getEndStop()


            if (ActivityCompat.checkSelfPermission(ActivityOne.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                writeDataToFileInternal(insertedBusses, fileName);
                writeDataToFileExternal(insertedBusses, fileName, extPath);
            } else {
                ActivityCompat.requestPermissions(ActivityOne.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_ID_ACCESS_STORAGE);
            }

        }
    }
    //END: ROOM DB Persistence TASKS in NEW THREAD


    //START: FILE Persistence preparation and tasks - External storage
    private void writeDataToFileExternal(List<BussDatabase> data, String fileName, String relPath) {
        if (checkExtStorage()[1]) {
            File storageDir = getExternalStorageDirectory();

            File dir = new File(storageDir.getAbsolutePath() + relPath);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir, fileName);

            try {
                FileOutputStream osWriter = new FileOutputStream(file);

                osWriter.write("--- Tu sa zacina zapis ---".getBytes());
                osWriter.write(("\n").getBytes());
                for (BussDatabase buss : data) {
                    osWriter.write(Long.toString(buss.getId()).getBytes());
                    osWriter.write(("\t" + buss.getTitle()).getBytes());
                    osWriter.write(("\t " + buss.getStartStop()).getBytes());//getStartStop()
                    osWriter.write(("\t" + buss.getEndStop()).getBytes());//getEndStop()
                    osWriter.write(("\n").getBytes());
                }
                osWriter.write("--- Tu sa konci zapis ---".getBytes());
                osWriter.write("\n".getBytes());
                osWriter.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
            }
        }
    }

    private boolean[] checkExtStorage() {
        boolean extStorageMounted = false;
        boolean extStorageCanWrite = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Read and write
            extStorageMounted = extStorageCanWrite = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // Only read
            extStorageMounted = true;
            extStorageCanWrite = false;
        } else {
            // No read, no write
            extStorageMounted = extStorageCanWrite = false;
        }
        return new boolean[]{extStorageMounted, extStorageCanWrite};
    }
    //END: FILE Persistence preparation and tasks - External Storage

    //START: FILE Persistence preparation and tasks - internal storage
    private void writeDataToFileInternal(List<BussDatabase> data, String fileName) {
        FileOutputStream outputStream;
        try {
            outputStream = openFileOutput(fileName, Context.MODE_PRIVATE);
            outputStream.write("--- Tu sa zacina zapis ---".getBytes());
            outputStream.write(("\n").getBytes());
            for (BussDatabase buss : data) {
                outputStream.write(Long.toString(buss.getId()).getBytes());
                outputStream.write(("\t" + buss.getTitle()).getBytes());
                outputStream.write(("\t " + buss.getStartStop()).getBytes());//getStartStop()
                outputStream.write(("\t" + buss.getEndStop()).getBytes());//getEndStop()
                outputStream.write(("\n").getBytes());
            }
            outputStream.write("--- Tu sa konci zapis ---".getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //What happends when the user grants or denies the requested permission to write to external storage
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_ID_ACCESS_STORAGE: {
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Toast.makeText(this, "Writing to file ...", Toast.LENGTH_LONG).show();
                            writeDataToFileInternal(insertedBusses, fileName);
                            writeDataToFileExternal(insertedBusses, fileName, extPath);
                        } else {
                            //In real cases, this string should not be hardcoded and would be places inside the values/strings.xml file
                            // Toast.makeText(this,"Unable to get permissions, have to quit.",Toast.LENGTH_LONG).show();
                            // finish();
                        }
                    }
                }
            }
        }
    }
    //END: FILE Persistence preparation and tasks

    //START: SharedPreferences persistence
    private void saveFirstBussNameToSharedPreferences(String key, String Title) {
        SharedPreferences sp = getSharedPreferences("sk.tuke.smartlab.lab3_datapersistence", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString(key, Title);
        ed.apply();
    }

    public void layoutChange() {
        //zmena layoutov
        (findViewById(R.id.imageButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(ActivityOne.this, MainActivity.class);
                startActivity(i);
            }
        });

        (findViewById(R.id.imageButton2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(ActivityOne.this, SecondActivity.class);
                startActivity(i);
            }
        });
        (findViewById(R.id.imageButton3)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(ActivityOne.this, ThirdActivity.class);
                startActivity(i);
            }
        });
    }


}
