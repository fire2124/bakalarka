package com.example.filip.schoolproject.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.filip.schoolproject.R;
import com.example.filip.schoolproject.RestApi.ApiMethods;
import com.example.filip.schoolproject.RestApi.ApiTools;
import com.example.filip.schoolproject.RestApi.Buss;
import com.google.gson.JsonObject;

import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DelayActivity extends AppCompatActivity  {
//implements LocationListener
    private List<Buss> zoznamBusov;
    public TextView locationText;
    public LocationManager locManager;
    public final int PERMISSION_LOCATION_ID = 1000;
    private final String CHANNEL_ID = "personal_notifications";
    private final int NOTIFICATION_ID = 1;

    public String[] number;
    public int count;
    Marker mark;
    IMapController controller;
    MapView map;
    int j;
    private final int PERMISSION_WRITE_EXT_STORAGE_ID = 1000;
    private final int PERMISSION_FINE_LOC_ID = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third_activity_traffic);
        layoutChange();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WRITE_EXT_STORAGE_ID);
        else
            //inicializacia mapy
            initialMapSetup();

        delay();
    }

    private void delay() {
        Thread thread = new Thread() {
            public void run() {
                new Timer().scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {

                        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).
                                baseUrl("https://mhdpresov.sk/").build();
                        ApiMethods api = retrofit.create(ApiMethods.class);
                        api.getGPSBusses().enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                zoznamBusov = ApiTools.getBussesFromApi(response.body(), DelayActivity.this);
                                for (j = 0; j < zoznamBusov.size(); j++) {
                                    try {
                                        controller.setCenter(new GeoPoint(zoznamBusov.get(j).getLat(), zoznamBusov.get(j).getLng()));//musi tu byt aby bolo viditelny pohyb vozidiel
                                        rog();
                                    } catch (NullPointerException e) {
                                        System.out.println(e);
                                    }
                                }
                                count++;
                                Intent intent = getIntent();
                                intent.putExtra("count", count);
                                Log.v("", "");
                                //showNotification();
                            }
                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                Toast.makeText(DelayActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }, 0, 15000);
            }
        };

        thread.start();
    }

    public void layoutChange() {

        //zmena layoutov
        (findViewById(R.id.imageButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(DelayActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

        (findViewById(R.id.imageButton2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(DelayActivity.this, SecondActivity.class);
                startActivity(i);
            }
        });
        (findViewById(R.id.imageButton3)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tu dam to co sa ma stat ked sa klikne na tlacidlo
                //Intent mi povie o tom co chcem urobit ... aktualne prejst z konextu aktivity MainActivity + chcem vytvorit novy kontext z aktivity FlightDetails
                Intent i = new Intent(DelayActivity.this, ThirdActivity.class);
                startActivity(i);
            }
        });
    }

    private void initialMapSetup() {
        map = findViewById(R.id.map_osm);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        controller = map.getController();
        controller.setZoom(14.0d);
        controller.setCenter(new GeoPoint(48.9970342, 21.2404656));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (map != null)
            map.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (map != null)
            map.onResume();
    }

    public void rog() {
        mark = new Marker(map);
        if (zoznamBusov.get(j).getDelay() > 0) {
            mark.setIcon(getResources().getDrawable(R.drawable.ic_launcher_background));
        } else if (zoznamBusov.get(j).getDelay() >= -120) {
            mark.setIcon(getResources().getDrawable(R.drawable.ic_launcher_background2));
        } else mark.setIcon(getResources().getDrawable(R.drawable.ic_launcher_background1));
        mark.setPosition(new GeoPoint(zoznamBusov.get(j).getLat(), zoznamBusov.get(j).getLng()));
        mark.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        map.getOverlays().add(mark);
    }
}
